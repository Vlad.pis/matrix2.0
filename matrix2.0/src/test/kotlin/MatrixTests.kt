package org.matrix

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.mockito.Mockito
import org.mockito.Mockito.mock

class MatrixTests {
    @Test
    fun `Test exception`() {
        val m = mock(Matrix::class.java)
        val ex = assertThrows<IllegalArgumentException> { fillMatrix(-1, m) }
        assertEquals("Oshibka", ex.message)
    }

    @Test
    fun `Test null`() {
        val m = mock(Matrix::class.java)
        val ex = assertThrows<IllegalArgumentException> { fillMatrix(0, m) }
        assertEquals("Oshibka", ex.message)
    }

    @Test
    fun `Mockito for 1x1`() {
        val m = mock(Matrix::class.java)

        fillMatrix(1, m)

        Mockito.verify(m).set(1, 1, 1)
    }

    @Test
    fun `Mockito for 4x4`() {
        val m = mock(Matrix::class.java)

        fillMatrix(4, m)

        Mockito.verify(m).set(1, 1, 1)
        Mockito.verify(m).set(2, 1, 2)
        Mockito.verify(m).set(3, 1, 3)
        Mockito.verify(m).set(4, 1, 4)
        Mockito.verify(m).set(4, 2, 5)
        Mockito.verify(m).set(4, 3, 6)
        Mockito.verify(m).set(4, 4, 7)
        Mockito.verify(m).set(3, 4, 8)
        Mockito.verify(m).set(2, 4, 9)
        Mockito.verify(m).set(1, 4, 10)
        Mockito.verify(m).set(1, 3, 11)
        Mockito.verify(m).set(2, 3, 12)
        Mockito.verify(m).set(3, 3, 13)
        Mockito.verify(m).set(3, 2, 14)
        Mockito.verify(m).set(2, 2, 15)
        Mockito.verify(m).set(1, 2, 16)
    }

    @Test
    fun `Mockito for 5x5`() {
        val m = mock(Matrix::class.java)

        fillMatrix(5, m)

        Mockito.verify(m).set(1, 1, 1)
        Mockito.verify(m).set(2, 1, 2)
        Mockito.verify(m).set(3, 1, 3)
        Mockito.verify(m).set(4, 1, 4)
        Mockito.verify(m).set(5, 1, 5)
        Mockito.verify(m).set(5, 2, 6)
        Mockito.verify(m).set(5, 3, 7)
        Mockito.verify(m).set(5, 4, 8)
        Mockito.verify(m).set(5, 5, 9)
        Mockito.verify(m).set(4, 5, 10)
        Mockito.verify(m).set(3, 5, 11)
        Mockito.verify(m).set(2, 5, 12)
        Mockito.verify(m).set(1, 5, 13)
        Mockito.verify(m).set(1, 4, 14)
        Mockito.verify(m).set(2, 4, 15)
        Mockito.verify(m).set(3, 4, 16)
        Mockito.verify(m).set(4, 4, 17)
        Mockito.verify(m).set(4, 3, 18)
        Mockito.verify(m).set(4, 2, 19)
        Mockito.verify(m).set(3, 2, 20)
        Mockito.verify(m).set(2, 2, 21)
        Mockito.verify(m).set(1, 2, 22)
        Mockito.verify(m).set(1, 3, 23)
        Mockito.verify(m).set(2, 3, 24)
        Mockito.verify(m).set(3, 3, 25)
    }
}
