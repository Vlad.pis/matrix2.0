/**
 * Вариант 14 СИБ-901
 * @param x - number of steps
 * @param dim - dimension of matrix
 * @param j - the difference of the arithmetic progression
 * @param v - the value for which you want to count the number
 * @param s - sum when we use recursion
 * @return sum of previous steps
 */

package org.matrix

interface Matrix {
    fun set(row: Int, col: Int, value: Int)
}

class Builder {
    private fun getSum(x: Int, dim: Int, j: Int, v: Int, s: Int): Int {
        if (v < 3 * dim - 2) return 0
        if (s < v - (3 * dim - 2 - j)) {
            return getSum(x + 1, dim, j + 4, v, s + (3 * dim - 2 - j))
        }
        return s
    }

    private fun getNumberOfStep(x: Int, dim: Int, j: Int, v: Int, s: Int): Int {
        if (v < 3 * dim - 2) return 0
        if (s < v - (3 * dim - 2 - j)) {
            return getNumberOfStep(x + 1, dim, j + 4, v, s + (3 * dim - 2 - j))
        }
        return x
    }

    private fun getNumberOfJ(x: Int, dim: Int, j: Int, v: Int, s: Int): Int {
        if (v < 3 * dim - 2) return 0
        if (s < v - (3 * dim - 2 - j)) {
            return getNumberOfJ(x + 1, dim, j + 4, v, s + (3 * dim - 2 - j))
        }
        return j
    }

    /**
     * Calculates number of row
     * @param dim - dimension of matrix
     * @param v - the value for which you want to count the number
     * @return the number of row
     */

    fun getRow(v: Int, dim: Int): Int {
        val sum = getSum(0, dim, 0, v, 0)
        val x = getNumberOfStep(0, dim, 0, v, 0)
        val j = getNumberOfJ(0, dim, 0, v, 0)
        var c = 0
        if (x == 1) {
            c = 2
        }
        if (x > 1 && dim % 2 == 1) {
            c = 2 + 3 * (x - 1)
        } else if (x > 1 && dim % 2 == 0) {
            c = 2 + 3 * (x - 1)
        }
        if (v <= dim - x + sum && v > sum) {
            return v - sum
        } else if (v <= 2 * (dim - 1) - c + sum && v > dim - x + sum) {
            return dim - x
        }
        return (3 * dim - 2 - j + sum) - v + 1
    }

    /**
     * Calculates number of column
     * @param dim - dimension of matrix
     * @param v - the value for which you want to count the number
     * @return the number of column
     */

    fun getColumn(v: Int, dim: Int): Int {

        val sum = getSum(0, dim, 0, v, 0)
        val x = getNumberOfStep(0, dim, 0, v, 0)
        var c = 0
        if (x == 1) {
            c = 2
        }
        if (x > 1) {
            c += 2 + 3 * (x - 1)
        }
        if (x % 2 == 1) {
            if (v <= dim - x + sum && v > sum) {
                return dim - x
            } else if (v <= 2 * (dim - 1) - c + sum && v > dim - x + sum) {
                return dim - x - (v - (dim - x + sum))
            }
            return x + 1
        }
        if (v <= dim - x + sum && v > sum) {
            return x + 1
        } else if (v <= 2 * (dim - 1) - c + sum && v > dim - x + sum) {
            return v - (dim - x + sum) + 1
        }
        return dim - x
    }
}

/**
 *  fillMatrix is a function that implements an algorithm to initialize a matrix that is filled by the algorithm.
 * The algorithm starts initialization from the first column, then the last row and the right column.
 * Then the same in reverse order
 * The matrix may look like this: for example, a 5 * 5 matrix
 *
 * 1 22 23 14 13
 *
 * 2 21 24 15 12
 *
 * 3 20 25 16 11
 *
 * 4 19 18 17 10
 *
 * 5  6  7  8  9
 *
 * getColumn - For the column number of a given value
 * getRow - For the row number of a given value
 * @param n is dimension of matrix
 */

fun fillMatrix(n: Int, m: Matrix) {
    val b = Builder()
    if (n <= 0) {
        throw IllegalArgumentException("Oshibka")
    }
    IntRange(1, n * n).forEach {
        m.set(b.getRow(it, n), b.getColumn(it, n), it)
    }
}
